print ( "entering ValidateSimpleEcho");

var schema = 
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "definitions": {},
  "required": [
    "Response"
  ],
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "Response": {
      "type": "string"
    }
  }
};

var valid = tv4.validate(JSON.parse(response.content), schema);
if(valid){
    print("Schema is valid!");
} else {

    var json = {};
    json.ResponseValidationError = tv4.error;
    context.setVariable("response.content", JSON.stringify(json));
    context.setVariable("response.status.code", 500);

  }